# Mind-Quant-Finance

在量化金融领域，需要使用数值计算方法对各类资产进行定价。本项目基于MindSpore，实现了资产定价模型，可充分利用CPU、GPU和Ascend提供的算力，完成数值模拟计算。 